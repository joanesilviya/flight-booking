create table flight(flight_id INT (10) not null primary key auto_increment ,
flight_number varchar(60) not null,
flight_name varchar(50) ,
flight_modal varchar(50) ,
total_seats int(10) default 100,
source varchar(50),
destination varchar(50),
date_created timestamp default now(),
date_updated timestamp
);


insert into flight(flight_number,flight_name,flight_modal,total_seats,source,destination,date_updated)
values('F001','Spice Jet','Boeing',80,'Hyderabad','Delhi',now());

insert into flight(flight_number,flight_name,flight_modal,total_seats,source,destination,date_updated)
values('AC1','Spice Jet','Embraer 175',80,'Delhi','Hyderabad',now());

insert into flight(flight_number,flight_name,flight_modal,source,destination,date_updated)
values('T1','Indigo','Boeing-777-300R','Chennai','Hyderabad',now());

insert into flight(flight_number,flight_name,flight_modal,source,destination,date_updated)
values('DJ1','Air Bus','Air Bus A318','Pune','Hyderabad',now());