package org.vapasi.flightbooking.model;

import java.util.List;

public class AjaxResponse {

    private String msg;
    private List<ScheduleResponseDetails> flightList;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ScheduleResponseDetails> getFlightList() {
        return flightList;
    }

    public void setFlightList(List<ScheduleResponseDetails> flightList) {
        this.flightList = flightList;
    }

}
