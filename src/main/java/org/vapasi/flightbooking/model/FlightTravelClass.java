package org.vapasi.flightbooking.model;

import org.vapasi.flightbooking.hcv.TravelClassType;

public class FlightTravelClass {

    private Integer flightTravelClassId;
    private Integer flightId;
    private TravelClassType travelClassType;
    private Integer noOfSeats;

    public FlightTravelClass(int flightTravelClassId, int flightId, TravelClassType travelClassType,
                             int noOfSeats){

        this.flightTravelClassId = flightTravelClassId;
        this.travelClassType = travelClassType;
        this.noOfSeats = noOfSeats;
        this.flightId = flightId;

    }

    public Integer getFlightTravelClassId() {
        return flightTravelClassId;
    }

    public void setFlightTravelClassId(Integer flightTravelClassId) {
        this.flightTravelClassId = flightTravelClassId;
    }

    public TravelClassType getTravelClassType() {
        return travelClassType;
    }

    public void setTravelClassType(TravelClassType travelClassType) {
        this.travelClassType = travelClassType;
    }

    public Integer getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(Integer noOfSeats) {
        this.noOfSeats = noOfSeats;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }



}
