package org.vapasi.flightbooking.model;

public class ScheduleResponseDetails {

    private Integer flightId;

    private Integer flightScheduleId;
    private String flightName;
    private String flightNumber;
    private String source;
    private String destination;
    private String startTime;
    private String endTime;
    private String perTicketPrice;
    private double totalPrice;
    private int noOfPassengers;
    private String travelClass;
    private String travelDate;

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPerTicketPrice() {
        return perTicketPrice;
    }

    public void setPerTicketPrice(String perTicketPrice) {
        this.perTicketPrice = perTicketPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(int noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public Integer getFlightScheduleId() {
        return flightScheduleId;
    }

    public void setFlightScheduleId(Integer flightScheduleId) {
        this.flightScheduleId = flightScheduleId;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }
}
