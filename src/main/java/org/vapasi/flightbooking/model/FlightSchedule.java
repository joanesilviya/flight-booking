package org.vapasi.flightbooking.model;

import java.util.Map;

public class FlightSchedule {

    private Integer flightScheduleId;
    private Integer flightId;
    private String source;
    private String destination;
    private String departureDate;
    private String startTime;
    private String destinationTime;
    private double cost;
    private Map<String,FlightScheduledClassFairDetails> travelFairDetails;
    private Map<String,FlightTravelClass> travelClassDetails;
    private Flight flight;


    public FlightSchedule(Integer flightScheduleId,Integer flightId, String source,
                          String destination, String departureDate, String startTime,String destinationTime){

        this.flightScheduleId = flightScheduleId;
        this.flightId = flightId;
        this.source = source;
        this.destination = destination;
        this.departureDate = departureDate;
        this.startTime = startTime;
        this.destinationTime = destinationTime;

    }


    public Integer getFlightScheduleId() {
        return flightScheduleId;
    }

    public void setFlightScheduleId(Integer flightScheduleId) {
        this.flightScheduleId = flightScheduleId;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDestinationTime() {
        return destinationTime;
    }

    public void setDestinationTime(String destinationTime) {
        this.destinationTime = destinationTime;
    }

    public Map<String, FlightScheduledClassFairDetails> getTravelFairDetails() {
        return travelFairDetails;
    }

    public void setTravelFairDetails(Map<String, FlightScheduledClassFairDetails> travelFairDetails) {
        this.travelFairDetails = travelFairDetails;
    }
    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Map<String, FlightTravelClass> getTravelClassDetails() {
        return travelClassDetails;
    }

    public void setTravelClassDetails(Map<String, FlightTravelClass> travelClassDetails) {
        this.travelClassDetails = travelClassDetails;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }
}
