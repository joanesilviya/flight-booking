package org.vapasi.flightbooking.model;

public class FlightScheduledClassFairDetails {
    private Integer flightScheduleClassFairId;
    private Integer flightScheduleId;
    private Integer flightTravelClassId;
    private int availableSeats;
    private double basePrice;
    private String travelClassType;

 public FlightScheduledClassFairDetails(Integer flightScheduleClassFairId,
                                           Integer flightScheduleId, String travelClassType, int availableSeats, double basePrice){

        this.flightScheduleClassFairId = flightScheduleClassFairId;
        this.flightScheduleId = flightScheduleId;
        this.travelClassType = travelClassType;
        this.availableSeats = availableSeats;
        this.basePrice = basePrice;
    }

    public Integer getFlightScheduleClassFairId() {
        return flightScheduleClassFairId;
    }

    public void setFlightScheduleClassFairId(Integer flightScheduleClassFairId) {
        this.flightScheduleClassFairId = flightScheduleClassFairId;
    }

    public Integer getFlightScheduleId() {
        return flightScheduleId;
    }

    public void setFlightScheduleId(Integer flightScheduleId) {
        this.flightScheduleId = flightScheduleId;
    }

    public Integer getFlightTravelClassId() {
        return flightTravelClassId;
    }

    public void setFlightTravelClassId(Integer flightTravelClassId) {
        this.flightTravelClassId = flightTravelClassId;
    }

    public Integer getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(Integer availableSeats) {
        this.availableSeats = availableSeats;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public String getTravelClassType() {
        return travelClassType;
    }

    public void setTravelClassType(String travelClassType) {
        this.travelClassType = travelClassType;
    }

}
