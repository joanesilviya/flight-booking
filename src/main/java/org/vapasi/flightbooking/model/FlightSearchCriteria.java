package org.vapasi.flightbooking.model;

import org.vapasi.flightbooking.hcv.TravelClassType;
import util.CalendarUtilities;

import javax.validation.constraints.NotEmpty;

public class FlightSearchCriteria {

    @NotEmpty (message = "From can't be empty!")
    String source;

    @NotEmpty (message = "Destination can't be empty!")
    String destination;

    int noOfSeats;

    String travelDate;

    String travelClass;

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public String getTravelClass() {
        return this.travelClass.isEmpty() ? TravelClassType.ECONOMY.name():this.travelClass;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.noOfSeats = noOfSeats;
    }

    public int getNoOfSeats() { return (noOfSeats ==0 ? 1 : noOfSeats); }

    public void setTravelDate(String travelDate) {this.travelDate = travelDate;}

    public String getTravelDate() {
        return (this.travelDate.isEmpty() ? CalendarUtilities.getTomorrow():this.travelDate);
    }

    @Override
    public String toString() {
        StringBuffer criteria = new StringBuffer(FlightSearchCriteria.class.getName())
                .append(": source = ").append(getSource()).append( "\n" )
                .append( " destination = " ).append( getDestination() ).append("\n")
                .append( " travel class = " ).append(getTravelClass()).append( "\n" )
                .append(" travel date = ").append(getTravelDate()).append("\n")
                .append("No of Passengers = ").append(getNoOfSeats());
        return criteria.toString();
    }
}
