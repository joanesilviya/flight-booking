package org.vapasi.flightbooking.model;

import java.util.Map;

public class Flight {

    private Integer flightId;
    private String flightNumber;
    private String flightName;
    private String flightModal;
    private int maxSeats;

    private Map<String,FlightTravelClass> travelClassDetails;
    private Map<Integer,FlightSchedule> flightSchedule;


    public Flight(Integer flightId, String flightNumber, String flightName, String flightModal, int maxSeats){

        this.flightId = flightId;
        this.flightNumber = flightNumber;
        this.flightName = flightName;
        this.flightModal = flightModal;

    }


    public Integer getFlightId() {
        return flightId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getFlightModal() {
        return flightModal;
    }

    public void setFlightModal(String flightModal) {
        this.flightModal = flightModal;
    }


    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }

    public Map<String, FlightTravelClass> getTravelClassDetails() {
        return travelClassDetails;
    }

    public void setTravelClassDetails(Map<String, FlightTravelClass> travelClassDetails) {
        this.travelClassDetails = travelClassDetails;
    }


    public Map<Integer, FlightSchedule> getFlightSchedule() {
        return flightSchedule;
    }

    public void setFlightSchedule(Map<Integer, FlightSchedule> flightSchedule) {
        this.flightSchedule = flightSchedule;
    }

}
