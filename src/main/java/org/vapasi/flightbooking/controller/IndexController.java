package org.vapasi.flightbooking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {


    @GetMapping("/flights")
    public String  showSearchFlight() {
        return "NewSearchFlight";
    }


}
