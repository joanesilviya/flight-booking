package org.vapasi.flightbooking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.vapasi.flightbooking.model.AjaxResponse;
import org.vapasi.flightbooking.model.FlightSearchCriteria;
import org.vapasi.flightbooking.model.ScheduleResponseDetails;
import org.vapasi.flightbooking.services.FlightServices;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FlightController {

    FlightServices flightServices;

    @Autowired
    public FlightController(FlightServices flightServices) {
        this.flightServices = flightServices;
    }


    //search flights by From, To and No of passengers
    //@CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping  (value = "/flight/searchflight")
    public ResponseEntity<AjaxResponse> searchFlights(@Valid @RequestBody FlightSearchCriteria searchCriteria, Errors errors){

            AjaxResponse result = new AjaxResponse();

            //If error, just return a 400 bad request, along with the error message
            if (errors.hasErrors()) {

                result.setMsg(errors.getAllErrors()
                        .stream().map(x -> x.getDefaultMessage())
                        .collect(Collectors.joining(",")));

                return ResponseEntity.badRequest().body(result);

            }

            List<ScheduleResponseDetails> flights = flightServices.searchFlight(searchCriteria);

            if(flights.size()>0){

                result.setFlightList(flights);
                result.setMsg( "SUCCESS" );
            }else{
                    result.setFlightList(null);
                result.setMsg(  "No flights found");
            }


        return  ResponseEntity.ok(result);
    }

    //Method to check JDBC connection
    @RequestMapping("/flightjdbc")
    public String displayJDBCResults(){
        flightServices.searchFromDB();
        return "Success";
    }

}
