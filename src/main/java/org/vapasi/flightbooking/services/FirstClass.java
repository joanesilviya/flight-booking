package org.vapasi.flightbooking.services;

import org.vapasi.flightbooking.repository.ITravelClass;
import util.CalendarUtilities;

public class FirstClass implements ITravelClass {

    @Override
    public double calculateTicketFair(int noOfTickets, int availableSeats,
                                      int allocatedSeatsForClass, int percentage, String travelDate, double basePrice) {

        double costOfTickets=0;
        int increasePercentage = 1;

        int days=0;
        days = CalendarUtilities.getDifferenceBetweenTwoDates( travelDate );
        if(days == 0 ){
            costOfTickets = noOfTickets * (basePrice +  ((basePrice*100)/100));

        }else if(days <=10){
            days = (10-days)+1;
            percentage = percentage*days;
            costOfTickets = noOfTickets * (basePrice + ((basePrice * percentage)/100));

        }else{
            costOfTickets =0.0;
        }

        return costOfTickets;
    }
}
