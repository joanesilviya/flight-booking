package org.vapasi.flightbooking.services;

import org.vapasi.flightbooking.hcv.BusinessClassDays;
import org.vapasi.flightbooking.repository.ITravelClass;
import util.CalendarUtilities;

import java.text.ParseException;

public class BusinessClass implements ITravelClass {


    @Override
    public double calculateTicketFair(int noOfTickets, int availableSeats, int allocatedSeatsForClass,
                                      int percentage, String travelDate, double basePrice) {

        double costOfTickets=0;
        String day="";


        try {

            day = CalendarUtilities.getDayForTheGivenDate( travelDate );

        }catch(ParseException pe){
            String bf = new StringBuffer("FlightServices :: searchFlight : ")
                    .append("calculateCostForBuisnessClass() ").append( ":: Date Parse Exception thrown ")
                    .append(pe.getMessage()).toString();
            System.out.println(bf);
        }

        //enum contains that particular
        if(BusinessClassDays.contains(BusinessClassDays.class,day)){
            costOfTickets = noOfTickets * (basePrice + ((basePrice * percentage)/100));
        }else{
            costOfTickets = noOfTickets * basePrice;
        }

        return costOfTickets;
    }
}
