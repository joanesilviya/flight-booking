package org.vapasi.flightbooking.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vapasi.flightbooking.hcv.TravelClassType;
import org.vapasi.flightbooking.model.*;
import org.vapasi.flightbooking.repository.FlightRepository;
import util.CalendarUtilities;

import java.util.*;

@Service
public class FlightServices {


    private FlightRepository flightRepository;


    @Autowired
    public FlightServices(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }


    //Search flights
    public List<ScheduleResponseDetails> searchFlight(FlightSearchCriteria searchCriteria) {

        Map<Integer,FlightSchedule> existingSchedules = flightRepository.getAllScheduledDetails();

        System.out.println("Search "+ searchCriteria.toString());
        int days=0;

        //checking the criteria
        Map<Integer,FlightSchedule> revisedCollection = new HashMap<Integer,FlightSchedule>();
        for (Iterator<Map.Entry<Integer, FlightSchedule>> it = existingSchedules.entrySet().iterator(); it.hasNext(); ) {

                FlightSchedule flightSchedule = it.next().getValue();


                    if(flightSchedule.getSource().equalsIgnoreCase( searchCriteria.getSource())
                    && flightSchedule.getDestination().equalsIgnoreCase( searchCriteria.getDestination() )
                    && flightSchedule.getDepartureDate().equals(searchCriteria.getTravelDate())
                    && flightSchedule.getTravelFairDetails().containsKey( searchCriteria.getTravelClass())){


                        revisedCollection.put(flightSchedule.getFlightScheduleId(),flightSchedule);
                    }
        }

        return populateValuesToFilteredCollection(revisedCollection,searchCriteria);
    }


    /*
    * This method is used to populate finalized data to the collection
     */
    private List<ScheduleResponseDetails> populateValuesToFilteredCollection(Map<Integer, FlightSchedule> revisedCollection,
                                                                            FlightSearchCriteria searchCriteria) {

        List<ScheduleResponseDetails> responseDetailsList = new ArrayList<ScheduleResponseDetails>();
        Iterator<Map.Entry<Integer, FlightSchedule>> scheduleIterator = revisedCollection.entrySet().iterator();
        ScheduleResponseDetails responseDetails;
        FlightSchedule flightSchedule;
        double costPerFlightSchedule =0.0;


        //pupulateFlightScheduleData(scheduleIterator);

        while (scheduleIterator.hasNext()){
            responseDetails = new ScheduleResponseDetails();
            flightSchedule = scheduleIterator.next().getValue();

            responseDetails.setFlightScheduleId( flightSchedule.getFlightScheduleId() );
            responseDetails.setFlightId( flightSchedule.getFlightId());
            responseDetails.setFlightName(flightSchedule.getFlight().getFlightName());
            responseDetails.setFlightNumber(flightSchedule.getFlight().getFlightNumber());

            responseDetails.setSource( flightSchedule.getSource() );
            responseDetails.setDestination( flightSchedule.getDestination() );
            responseDetails.setStartTime( flightSchedule.getStartTime() );
            responseDetails.setEndTime( flightSchedule.getDestinationTime() );
            responseDetails.setTravelDate(flightSchedule.getDepartureDate());
            responseDetails.setNoOfPassengers(searchCriteria.getNoOfSeats());
            responseDetails.setTravelClass( searchCriteria.getTravelClass());

            FlightScheduledClassFairDetails fairDetails = flightSchedule.getTravelFairDetails()
                    .get(searchCriteria.getTravelClass());

            Map<String,FlightTravelClass> flightTravelClassCollection = flightSchedule.getTravelClassDetails();

            //Economy
            if(searchCriteria.getTravelClass().equals( TravelClassType.ECONOMY.name() )) {
                FlightTravelClass ftc = flightTravelClassCollection.get(TravelClassType.ECONOMY.name());

                EconomyClass economyClass = new EconomyClass();
                costPerFlightSchedule = economyClass.calculateTicketFair(searchCriteria.getNoOfSeats(),fairDetails.getAvailableSeats(),
                        ftc.getNoOfSeats(),0,"", fairDetails.getBasePrice());
                responseDetails.setTotalPrice( costPerFlightSchedule );
            }

            //First class
            if(searchCriteria.getTravelClass().equals( TravelClassType.FIRSTCLASS.name() )) {
                FirstClass firstClass = new FirstClass();
                costPerFlightSchedule = firstClass.calculateTicketFair( searchCriteria.getNoOfSeats(),fairDetails.getAvailableSeats(),
                        0,10,searchCriteria.getTravelDate(), fairDetails.getBasePrice());
                responseDetails.setTotalPrice( costPerFlightSchedule );
            }

            //Business class
            if(searchCriteria.getTravelClass().equals( TravelClassType.BUSINESS.name() )) {
                BusinessClass businessClass = new BusinessClass();
                costPerFlightSchedule = businessClass.calculateTicketFair( searchCriteria.getNoOfSeats(),fairDetails.getAvailableSeats(),
                        0,40,searchCriteria.getTravelDate(), fairDetails.getBasePrice());
                responseDetails.setTotalPrice( costPerFlightSchedule );
            }

            //append to the collection
            responseDetailsList.add(responseDetails);
            costPerFlightSchedule=0.0;
        }

        return responseDetailsList;
    }


    //test method to fetch from database
    public void searchFromDB(){
        flightRepository.getAllFlightsUsingJDBC();
    }

}
