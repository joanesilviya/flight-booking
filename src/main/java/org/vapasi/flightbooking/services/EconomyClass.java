package org.vapasi.flightbooking.services;

import org.vapasi.flightbooking.repository.ITravelClass;

/*
*
*/
public class EconomyClass implements ITravelClass {

    @Override
    public double calculateTicketFair(int noOfTickets, int availableSeats, int allocatedSeats,
                                      int percentage, String travelDate, double basePrice) {
        double costOfTickets=0;
        double first40Percent = 0;
        double second50Percent =0;
        double third10Percent =0;

        first40Percent = Math.round((double) allocatedSeats*40 /100);
        second50Percent = Math.round((double) allocatedSeats*50 /100);
        third10Percent = Math.round((double) allocatedSeats*10 /100);

        int bookedSeats = allocatedSeats-availableSeats;
        int seatsTobeBooked = bookedSeats;

        //first 40% seats
        int range1=1;
        int range2=(int)first40Percent;


        for(int start=1;start<=noOfTickets;start++){
            seatsTobeBooked = seatsTobeBooked + start;

            System.out.println("SeatsTObeBooked = "+ seatsTobeBooked);

            if(seatsTobeBooked>=range1 && seatsTobeBooked<=range2){
                //it falls under first category
                System.out.println("Falls under first 40%");
                costOfTickets +=  basePrice ;
            }

            range1 =  range2+1;
            range2 = range2+(int)second50Percent;
            if(seatsTobeBooked>=range1 && seatsTobeBooked<=range2){
                //it falls under second category
                System.out.println("Falls under second 50%");
                costOfTickets +=  basePrice + (basePrice * 30)/100;
            }

            range1 =  range2+1;
            range2 = range2+(int)third10Percent;
            if(seatsTobeBooked>=range1 && seatsTobeBooked<range2){
                System.out.println("Falls under last 10%");
                System.out.println("starting range = "+ range1);
                System.out.println("ending range = "+ range2);

                costOfTickets += basePrice + (basePrice * 60)/100;
            }

            seatsTobeBooked = bookedSeats;
            range1=1;
            range2=(int)first40Percent;

        }

        costOfTickets = costOfTickets* noOfTickets;
        System.out.println("Total Cost of tickets = "+costOfTickets);

        return costOfTickets;    }
}
