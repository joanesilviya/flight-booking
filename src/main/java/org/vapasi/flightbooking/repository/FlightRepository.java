package org.vapasi.flightbooking.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.vapasi.flightbooking.hcv.SeedFlightObject;
import org.vapasi.flightbooking.model.Flight;
import org.vapasi.flightbooking.model.FlightSchedule;
import org.vapasi.flightbooking.model.FlightScheduledClassFairDetails;
import org.vapasi.flightbooking.model.FlightTravelClass;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class FlightRepository implements ObjectRepository {


    @Autowired
    JdbcTemplate jdbcTemplate;


    /*
     * Get all the flights
     */
    public List<Flight> getAllFlights(){
        List<Flight> existingFlights = SeedFlightObject.getAllFlights();
        for(Flight flight: existingFlights){
            flight.setTravelClassDetails(getAllFlightTravelClassByFlightId(flight.getFlightId()));
            flight.setFlightSchedule(getAllScheduledDetailsByFlightId(flight.getFlightId()));
        }

        return existingFlights;
    }


    //returns flight details by flightId
    public Flight getFlightDetailsByFlightId(Integer flightId){
        List<Flight> existingFlights = SeedFlightObject.getAllFlights();
        Flight flight = existingFlights.stream().filter( f -> f.getFlightId() == flightId ).findFirst().get();
        return flight;

    }


    //returns the collection of travel class details of a flight
        public Map<String,FlightTravelClass>  getAllFlightTravelClassByFlightId(Integer flightId){

        List<FlightTravelClass> existingTravelClass = SeedFlightObject.getAllTravelClassForAllFlights();
        Map<String,FlightTravelClass>  travelClassDetailsForaFlight = new HashMap<String,FlightTravelClass>();

        for(FlightTravelClass tc : existingTravelClass){
            if(tc.getFlightId() == flightId){
                travelClassDetailsForaFlight.put(tc.getTravelClassType().name(),tc);
            }

        }
        return travelClassDetailsForaFlight;
    }

    //Get All Schedule Details
    public Map<Integer,FlightSchedule>  getAllScheduledDetails(){

        List<FlightSchedule> allScheduledFlights = SeedFlightObject.getAllScheduledFlights();
        Map<Integer, FlightSchedule>  scheduledDetails = new HashMap<Integer,FlightSchedule>();

        for(FlightSchedule fs : allScheduledFlights){
                //TravelFairDetails
                fs.setTravelFairDetails(getFairDetailsForScheduledFlight(fs.getFlightScheduleId()));

                //travelClassDetails
                 fs.setTravelClassDetails( getAllFlightTravelClassByFlightId(fs.getFlightId()) );

                 //flightDetails
                    fs.setFlight( getFlightDetailsByFlightId( fs.getFlightId() ) );

                 //scheduleDetails
                scheduledDetails.put(fs.getFlightScheduleId(),fs);
        }


        return scheduledDetails;
    }

    //returns all the flights schedules
    private Map<String, FlightScheduledClassFairDetails>  getFairDetailsForScheduledFlight(Integer flightScheduleId){

        Map<String, FlightScheduledClassFairDetails>  fairDetailsForaSchedule = new HashMap<String,FlightScheduledClassFairDetails>();

        //Get Fair Details for Schedule
        List<FlightScheduledClassFairDetails> allClassFairDetailsForScheduledFlights = SeedFlightObject.getAllClassFairDetailsForScheduledFlights();

        for(FlightScheduledClassFairDetails fs : allClassFairDetailsForScheduledFlights){
            if(fs.getFlightScheduleId() == flightScheduleId){
                    fairDetailsForaSchedule.put(fs.getTravelClassType(),fs);
            }

        }
        return fairDetailsForaSchedule;
    }

    //Test method to get connected to a database
    public Map<Integer, Flight> getAllFlightsUsingJDBC(){
        String query="select * from flight;";
        List<Map<String, Object>> objects = jdbcTemplate.queryForList(query);

        Map<Integer, Flight> flightObjectMap = new HashMap<Integer, Flight>();
        for (Map<String,Object> f: objects) {
            System.out.println(f.get("id")+" Name "+ f.get( "name" ));
        }

        return flightObjectMap;

    }

    //Get all flights schedules
    private Map<Integer,FlightSchedule>  getAllScheduledDetailsByFlightId(Integer flightId){

        List<FlightSchedule> allScheduledFlights = SeedFlightObject.getAllScheduledFlights();
        Map<Integer, FlightSchedule>  scheduledDetails = new HashMap<Integer,FlightSchedule>();

        for(FlightSchedule fs : allScheduledFlights){
            if(fs.getFlightId() == flightId){
                fs.setTravelFairDetails(getFairDetailsForScheduledFlight(fs.getFlightScheduleId()));
                scheduledDetails.put(fs.getFlightScheduleId(),fs);
            }
        }
        return scheduledDetails;
    }
}
