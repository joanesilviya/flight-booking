package org.vapasi.flightbooking.repository;

import java.util.List;

public interface ObjectRepository<T> {

    public T getAllFlights() ;
}
