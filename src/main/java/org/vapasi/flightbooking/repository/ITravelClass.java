package org.vapasi.flightbooking.repository;

/*
* This interface is included for calculating the cost of ticket fair for all Travel Class Types(Business,Economy,First Class)
* having in mind in future if the percentage changes for classes or if any new Class Type is introduced then
* the code can be easily extended. And will be easy for any developer to modify the existing code.
* @author Joane Silviya
*  */
public interface ITravelClass {

    /*
     * This method has needed parameters for all Travel Class types. Some are not specific to the classes,
     * but if needed for extension it will be easy to modify and maintain.
     *
     */
    double calculateTicketFair(int noOfTickets, int availableSeats, int allocatedSeatsForClass,
                               int percentage, String travelDate, double basePrice);
}
