package org.vapasi.flightbooking.hcv;

import org.springframework.stereotype.Service;
import org.vapasi.flightbooking.model.Flight;
import org.vapasi.flightbooking.model.FlightSchedule;
import org.vapasi.flightbooking.model.FlightScheduledClassFairDetails;
import org.vapasi.flightbooking.model.FlightTravelClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SeedFlightObject {

    //load list of flights
    public static List<Flight> getAllFlights(){

        List<Flight> flights = new ArrayList<Flight>();


        flights.add(new Flight( 1,"AC1","Spice Jet" ,"Boeing",200));
        flights.add(new Flight( 2,"T1","Indigo" ,"Embraer",250));
        flights.add(new Flight( 3,"F003","Air Bus" ,"Boeing-777-300R",300));

        return flights;
    }

    //load list of flights
    public static List<FlightSchedule> getAllScheduledFlights(){

        List<FlightSchedule> flightSchedule = new ArrayList<FlightSchedule>();


        flightSchedule.add(new FlightSchedule( 1,1,"Hyderabad","Delhi","08/29/2019" ,"08:00:00","10:05:00"));
        flightSchedule.add(new FlightSchedule( 2,2,"Hyderabad","Delhi","08/29/2019","20:05:00","21:05:00"));

        flightSchedule.add(new FlightSchedule( 3,3,"Hyderabad","Delhi","09/20/2019" ,"18:00:00","19:05:00"));

        flightSchedule.add(new FlightSchedule( 4,2,"Chennai","Hyderabad","09/07/2019","12:30:00","13:30:00"));
        flightSchedule.add(new FlightSchedule( 5,2,"Hyderabad","Chennai","02/09/2019","15:30:00","16:30:00"));

        flightSchedule.add(new FlightSchedule( 6,3,"Hyderabad","Delhi","08/30/2019","20:05:00","22:30:00"));

        return flightSchedule;
    }

    public static List<FlightTravelClass> getAllTravelClassForAllFlights(){

        List<FlightTravelClass> flightTravelClassMap = new ArrayList<FlightTravelClass>();

        flightTravelClassMap.add(new FlightTravelClass(1,1,TravelClassType.ECONOMY,100) );
        flightTravelClassMap.add(new FlightTravelClass(2,1,TravelClassType.FIRSTCLASS,50) );
        flightTravelClassMap.add(new FlightTravelClass(3,1,TravelClassType.BUSINESS,50) );

        flightTravelClassMap.add(new FlightTravelClass(4,2,TravelClassType.ECONOMY,200) );
        flightTravelClassMap.add(new FlightTravelClass(5,2,TravelClassType.FIRSTCLASS,25) );
        flightTravelClassMap.add(new FlightTravelClass(6,2,TravelClassType.BUSINESS,25) );

        flightTravelClassMap.add(new FlightTravelClass(7,3,TravelClassType.ECONOMY,200) );
        flightTravelClassMap.add(new FlightTravelClass(8,3,TravelClassType.FIRSTCLASS,50) );
        flightTravelClassMap.add(new FlightTravelClass(9,3,TravelClassType.BUSINESS,50) );

        return flightTravelClassMap;
    }


    public static List<FlightScheduledClassFairDetails> getAllClassFairDetailsForScheduledFlights(){

        List<FlightScheduledClassFairDetails>  scheduledClassFair= new ArrayList<FlightScheduledClassFairDetails>();

        scheduledClassFair.add(new FlightScheduledClassFairDetails(1,1,"ECONOMY",70,2000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(2,1,"FIRSTCLASS",10,7000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(3,1,"BUSINESS",20,6000));

        scheduledClassFair.add(new FlightScheduledClassFairDetails(4,2,"ECONOMY",50,3000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,2,"FIRSTCLASS",25,6000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,2,"BUSINESS",25,4000));

        scheduledClassFair.add(new FlightScheduledClassFairDetails(4,3,"ECONOMY",50,3000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,3,"FIRSTCLASS",25,6000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,3,"BUSINESS",25,4000));

        scheduledClassFair.add(new FlightScheduledClassFairDetails(4,4,"ECONOMY",50,3000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,4,"FIRSTCLASS",25,6000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,4,"BUSINESS",25,5000));

        scheduledClassFair.add(new FlightScheduledClassFairDetails(4,6,"ECONOMY",50,3000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,6,"FIRSTCLASS",25,7000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,6,"BUSINESS",25,5000));

        scheduledClassFair.add(new FlightScheduledClassFairDetails(4,5,"ECONOMY",50,3000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,5,"FIRSTCLASS",25,7000));
        scheduledClassFair.add(new FlightScheduledClassFairDetails(5,5,"BUSINESS",25,5000));

        return scheduledClassFair;
    }
}
