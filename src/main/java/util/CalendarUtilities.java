package util;

import java.io.Console;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class CalendarUtilities {

    /*
     * This method returns the day of the week
     */
    public static String getDayForTheGivenDate(String dateToBeFormated) throws ParseException {
        SimpleDateFormat format1=new SimpleDateFormat("dd/MM/yyyy");
        Date dt1=format1.parse(dateToBeFormated);
        DateFormat format2=new SimpleDateFormat("EEEE");
        String finalDay=format2.format(dt1);
        System.out.println(finalDay);
        return finalDay;
    }


    /*
     *  This method is used to get set the default travel date
     */
    public static String getTomorrow(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDateTime today =  LocalDateTime.now();     //Today
        LocalDateTime tomorrow = today.plusDays(1);     //Add one day to it
        return dtf.format(tomorrow);
    }

    /*
     * This method is used to get the difference between todays and travel date
     */
    public static int getDifferenceBetweenTwoDates(String dateToBeFormated){
        int differenceInDays =0;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localTravelDate = LocalDate.parse(dateToBeFormated,dtf); //set the format for travel date
        LocalDate nowDate = LocalDate.now(); //today's date

        Period diff = Period.between(nowDate,localTravelDate);
        differenceInDays = diff.getDays();
        System.out.println("Difference between two dates = "+ differenceInDays);
        return differenceInDays;
    }
}
