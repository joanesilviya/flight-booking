$(document).ready(function () {

    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

    //jQuery Datepicker
    // Datepicker Popups calender to Choose date.
    $(function() {
        $("#travelDate").datepicker();
        // Pass the user selected date format.
        //$("#format").change(function() {
          //  $("#datepicker").datepicker("option", "dateFormat", $(this).val());
            //alert(''+($(this).val()));
        //});
    });


});

function fire_ajax_submit() {

    var search = {}
    search["source"] = $("#source").val();
    search["destination"] = $("#destination").val();
    search["noOfSeats"] = $("#noOfSeats").val();
    search["travelDate"] = $("#travelDate").val();
    search["travelClass"] = $("#travelClass").val();

    $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/flight/searchflight",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {


                if(! ($('#flightTable tbody').empty())){
                    $('#flightTable tbody').remove();
                }

                renderFLightData(data);

        },
        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#noFlightDiv').html(json);

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });


}

function renderFLightData(data){

            var flightDetails = data.flightList;
            var tr;



            if(data.msg == "SUCCESS"){
                document.getElementById("noFlightDiv").style.display = "none";
                document.getElementById("flightDiv").style.display = "block";

                    for (var i = 0; i < flightDetails.length; i++) {
                           var flight = flightDetails[i];
                           tr = $('<tr/>');
                           tr.append("<td>" + flight.flightName + "</td>");
                           tr.append("<td>" + flight.travelDate + "</td>");
                           tr.append("<td>" + flight.startTime+" - "+flight.endTime+"</td>")
                           tr.append("<td>" + flight.travelClass + "</td>");
                           tr.append("<td>" + flight.noOfPassengers + "</td>");
                           tr.append("<td>" + flight.totalPrice + "</td>");
                           tr.append("<td><a href='javascript:AdminInvoiceView();'><img alt='View' src='images/cim/icon/ViewFile.gif' /></a> </td>");

                           $('#flightTable tbody').append(tr);
                           tr="";
                       }

                console.log("SUCCESS : ", data.flightList);
                $("#btn-search").prop("disabled", false);
            }else{
                   var $flightDiv = $('#flightDiv');
                   var $msgDiv = $('#noFlightDiv');
                   $flightDiv.hide();
                   $msgDiv.show();
                   $msgDiv.show().html(data.msg.fontcolor('Red').fontsize(5));
            }
}