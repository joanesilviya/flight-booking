package flight;

import org.junit.jupiter.api.Test;
import org.vapasi.flightbooking.hcv.TravelClassType;
import org.vapasi.flightbooking.model.*;
import org.vapasi.flightbooking.repository.FlightRepository;
import org.vapasi.flightbooking.services.FlightServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SearchFlightTest {

    @Test
    public void shouldReturnFlightsWhenNumberOfPassengersIsNotGiven() {

        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        Map<String,FlightTravelClass> travelClassDetails = new HashMap<String,FlightTravelClass>();

        Flight flight = new Flight( 1,"AC1","Spice Jet" ,"Boeing",200);


        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/29/2019", "08:00:00", "10:05:00" );

        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 70, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );
        fs.setFlight( flight );

        //travelClassDetails
        travelClassDetails.put(TravelClassType.ECONOMY.name(),new FlightTravelClass(1,1,TravelClassType.ECONOMY,100) );
        travelClassDetails.put(TravelClassType.FIRSTCLASS.name(),new FlightTravelClass(2,1,TravelClassType.FIRSTCLASS,50) );
        travelClassDetails.put(TravelClassType.BUSINESS.name(), new FlightTravelClass(3,1,TravelClassType.BUSINESS,50) );
        fs.setTravelClassDetails( travelClassDetails );
        existingScheduledFlights.put( 1, fs );

        fs = new FlightSchedule( 2, 1, "Hyderabad", "Delhi", "08/29/2019", "20:05:00", "21:05:00" );
        classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 4, 2, "ECONOMY", 50, 3000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 5, 2, "FIRSTCLASS", 25, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 5, 2, "BUSINESS", 25, 4000 ) );
        fs.setTravelFairDetails( classFairDetails );
        fs.setFlight( flight );

        travelClassDetails = new HashMap<String,FlightTravelClass>();
        //travelClassDetails
        travelClassDetails.put(TravelClassType.ECONOMY.name(),new FlightTravelClass(1,1,TravelClassType.ECONOMY,100) );
        travelClassDetails.put(TravelClassType.FIRSTCLASS.name(),new FlightTravelClass(2,1,TravelClassType.FIRSTCLASS,50) );
        travelClassDetails.put(TravelClassType.BUSINESS.name(), new FlightTravelClass(3,1,TravelClassType.BUSINESS,50) );
        fs.setTravelClassDetails( travelClassDetails );
        existingScheduledFlights.put( 2, fs );


        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "08/29/2019" );
        searchCriteria.setTravelClass( TravelClassType.ECONOMY.name() );
        searchCriteria.setNoOfSeats( 0 );

        List<ScheduleResponseDetails> objects = flightServices.searchFlight( searchCriteria );
        assertEquals( 2, objects.size() );
    }


    @Test
    public void shouldReturnFlightsForNextDayWhenTravelDateIsNotGiven() {
        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/24/2019", "08:00:00", "10:05:00" );

        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 70, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 1, fs );

        fs = new FlightSchedule( 2, 1, "Hyderabad", "Delhi", "08/24/2019", "20:05:00", "21:05:00" );
        classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 4, 2, "ECONOMY", 50, 3000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 5, 2, "FIRSTCLASS", 25, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 5, 2, "BUSINESS", 25, 4000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 2, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "" );
        searchCriteria.setTravelClass( TravelClassType.ECONOMY.name() );
        searchCriteria.setNoOfSeats( 2 );

        List<ScheduleResponseDetails> objects = flightServices.searchFlight( searchCriteria );
        assertEquals( 2, objects.size() );

    }

    @Test
    public void shouldReturnFlightsWhenTravelClassIsNotGiven() {
        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/24/2019", "08:00:00", "10:05:00" );

        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 70, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 1, fs );

        fs = new FlightSchedule( 2, 1, "Hyderabad", "Delhi", "08/24/2019", "20:05:00", "21:05:00" );
        classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 4, 2, "ECONOMY", 50, 3000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 5, 2, "FIRSTCLASS", 25, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 5, 2, "BUSINESS", 25, 4000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 2, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "08/24/2019" );
        searchCriteria.setTravelClass( "" );
        searchCriteria.setNoOfSeats( 2 );

        List<ScheduleResponseDetails> objects = flightServices.searchFlight( searchCriteria );
        assertEquals( 2, objects.size() );
    }

    @Test
    public void shouldReturnFlightsWhenNumberOfPassengersAndDateOfDepartureNotGiven() {
        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/24/2019", "08:00:00", "10:05:00" );

        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 70, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 1, fs );

        fs = new FlightSchedule( 2, 1, "Hyderabad", "Delhi", "08/24/2019", "20:05:00", "21:05:00" );
        classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 4, 2, "ECONOMY", 50, 3000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 5, 2, "FIRSTCLASS", 25, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 5, 2, "BUSINESS", 25, 4000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 2, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "" );
        searchCriteria.setTravelClass( TravelClassType.ECONOMY.name() );
        searchCriteria.setNoOfSeats( 0 );

        List<ScheduleResponseDetails> objects = flightServices.searchFlight( searchCriteria );
        assertEquals( 2, objects.size() );

    }


    @Test
    public void shouldReturnFlightsWhenAllOptionalCriteriasNotGiven() {

        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/24/2019", "08:00:00", "10:05:00" );

        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 70, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 1, fs );

        fs = new FlightSchedule( 2, 1, "Hyderabad", "Delhi", "08/24/2019", "20:05:00", "21:05:00" );
        classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 4, 2, "ECONOMY", 50, 3000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 5, 2, "FIRSTCLASS", 25, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 5, 2, "BUSINESS", 25, 4000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 2, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "" );
        searchCriteria.setTravelClass( "" );
        searchCriteria.setNoOfSeats( 0 );

        List<ScheduleResponseDetails> objects = flightServices.searchFlight( searchCriteria );
        assertEquals( 2, objects.size() );
    }

    @Test
    public void shouldReturnFlightsWhenAllOptionalCriteriasGiven() {

        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/26/2019", "08:00:00", "10:05:00" );

        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 70, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 1, fs );

        fs = new FlightSchedule( 2, 1, "Hyderabad", "Delhi", "08/26/2019", "20:05:00", "21:05:00" );
        classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 4, 2, "ECONOMY", 50, 3000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 5, 2, "FIRSTCLASS", 25, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 5, 2, "BUSINESS", 25, 4000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 2, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "08/26/2019" );
        searchCriteria.setTravelClass( "BUSINESS" );
        searchCriteria.setNoOfSeats( 0 );

        List<ScheduleResponseDetails> objects = flightServices.searchFlight( searchCriteria );
        assertEquals( 2, objects.size() );
    }

    @Test
    public void shouldReturnCostOfTicketsGivenCriteriaForFirstClass() {

        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "09/03/2019", "08:00:00", "10:05:00" );

        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 20, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 1, fs );

        fs = new FlightSchedule( 2, 1, "Hyderabad", "Delhi", "09/03/2019", "20:05:00", "21:05:00" );
        classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 4, 2, "ECONOMY", 40, 3000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 5, 2, "FIRSTCLASS", 25, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 5, 2, "BUSINESS", 25, 4000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 2, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "09/03/2019" );
        searchCriteria.setTravelClass( TravelClassType.FIRSTCLASS.name() );
        searchCriteria.setNoOfSeats( 2 );

        List<ScheduleResponseDetails> results = flightServices.searchFlight( searchCriteria );
        assertThat(results).hasSize(2);
        assertThat(results).extracting("flightScheduleId").containsExactly(1,2);
        assertThat(results).extracting("totalPrice").containsExactly(12000.0,12000.0);

    }


    @Test
    public void shouldReturnCostOfTicketsForBusinessClassForSpecificDays() {

        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/27/2019", "08:00:00", "10:05:00" );

        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 20, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 1, fs );

        fs = new FlightSchedule( 2, 1, "Hyderabad", "Delhi", "08/27/2019", "20:05:00", "21:05:00" );
        classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 4, 2, "ECONOMY", 40, 3000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 5, 2, "FIRSTCLASS", 25, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 5, 2, "BUSINESS", 25, 4000 ) );
        fs.setTravelFairDetails( classFairDetails );
        existingScheduledFlights.put( 2, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "08/27/2019" );
        searchCriteria.setTravelClass( TravelClassType.BUSINESS.name() );
        searchCriteria.setNoOfSeats( 2 );

        List<ScheduleResponseDetails> objects = flightServices.searchFlight( searchCriteria );
        double cost1 = 0;
        if (objects != null && objects.size() > 0) {
            cost1 = objects.stream().findFirst().get().getTotalPrice();
        }
        assertEquals( 16800.0, cost1 );



    }


    @Test
    public void shouldReturnBasePriceForFirst40PercentCategoryForEconomy() {

        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        Map<String, FlightTravelClass> travelClassDetails = new HashMap<String, FlightTravelClass>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/26/2019", "08:00:00", "10:05:00" );

        //fairClassDetails
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 150, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );

        //travelClassDetails
        travelClassDetails.put(TravelClassType.ECONOMY.name(),new FlightTravelClass(1,1,TravelClassType.ECONOMY,200) );
        travelClassDetails.put(TravelClassType.FIRSTCLASS.name(),new FlightTravelClass(2,1,TravelClassType.FIRSTCLASS,10) );
        travelClassDetails.put(TravelClassType.BUSINESS.name(), new FlightTravelClass(3,1,TravelClassType.BUSINESS,20) );
        fs.setTravelClassDetails( travelClassDetails );
        existingScheduledFlights.put( 1, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "08/26/2019" );
        searchCriteria.setTravelClass( TravelClassType.ECONOMY.name() );
        searchCriteria.setNoOfSeats( 2 );

        List<ScheduleResponseDetails> results = flightServices.searchFlight( searchCriteria );
        assertThat(results).hasSize(1);
        assertThat(results).extracting("flightScheduleId").containsExactly(1);
        assertThat(results).extracting("totalPrice").containsExactly(8000.0);

    }

    @Test
    public void shouldReturnAdditional30PercentOfBasePriceForEconomyFor50PercentSeats() {

        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        Map<String, FlightTravelClass> travelClassDetails = new HashMap<String, FlightTravelClass>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/26/2019", "08:00:00", "10:05:00" );

        //fairClassDetails
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 40, 3000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );

        //travelClassDetails
        travelClassDetails.put(TravelClassType.ECONOMY.name(),new FlightTravelClass(1,1,TravelClassType.ECONOMY,200) );
        travelClassDetails.put(TravelClassType.FIRSTCLASS.name(),new FlightTravelClass(2,1,TravelClassType.FIRSTCLASS,10) );
        travelClassDetails.put(TravelClassType.BUSINESS.name(), new FlightTravelClass(3,1,TravelClassType.BUSINESS,20) );
        fs.setTravelClassDetails( travelClassDetails );
        existingScheduledFlights.put( 1, fs );

        fs = new FlightSchedule( 2, 1, "Hyderabad", "Delhi", "08/26/2019", "20:05:00", "21:05:00" );
        classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 4, 2, "ECONOMY", 40, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 5, 2, "FIRSTCLASS", 25, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 5, 2, "BUSINESS", 25, 4000 ) );
        fs.setTravelFairDetails( classFairDetails );

        travelClassDetails.put(TravelClassType.ECONOMY.name(),new FlightTravelClass(4,2,TravelClassType.ECONOMY,100) );
        travelClassDetails.put(TravelClassType.FIRSTCLASS.name(),new FlightTravelClass(5,2,TravelClassType.FIRSTCLASS,25) );
        travelClassDetails.put(TravelClassType.BUSINESS.name(),new FlightTravelClass(6,2,TravelClassType.BUSINESS,25) );
        fs.setTravelClassDetails( travelClassDetails );
        existingScheduledFlights.put( 2, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "08/26/2019" );
        searchCriteria.setTravelClass( TravelClassType.ECONOMY.name() );
        searchCriteria.setNoOfSeats( 2 );

        List<ScheduleResponseDetails> results = flightServices.searchFlight( searchCriteria );
        assertThat(results).hasSize(2);
        assertThat(results).extracting("flightScheduleId").containsExactly(1,2);
        assertThat(results).extracting("totalPrice").containsExactly(15600.0,10400.0);

    }


    @Test
    public void shouldReturnAdditional60PercentOfBasePriceForEconomyFor10PercentSeats() {

        Map<Integer, FlightSchedule> existingScheduledFlights = new HashMap<Integer, FlightSchedule>();
        FlightSchedule fs;
        Map<String, FlightScheduledClassFairDetails> classFairDetails = new HashMap<String, FlightScheduledClassFairDetails>();
        Map<String, FlightTravelClass> travelClassDetails = new HashMap<String, FlightTravelClass>();

        fs = new FlightSchedule( 1, 1, "Hyderabad", "Delhi", "08/26/2019", "08:00:00", "10:05:00" );

        //fairClassDetails
        classFairDetails.put( TravelClassType.ECONOMY.name(), new FlightScheduledClassFairDetails( 1, 1, "ECONOMY", 8, 2000 ) );
        classFairDetails.put( TravelClassType.FIRSTCLASS.name(), new FlightScheduledClassFairDetails( 2, 1, "FIRSTCLASS", 10, 4000 ) );
        classFairDetails.put( TravelClassType.BUSINESS.name(), new FlightScheduledClassFairDetails( 3, 1, "BUSINESS", 20, 6000 ) );
        fs.setTravelFairDetails( classFairDetails );

        //travelClassDetails
        travelClassDetails.put(TravelClassType.ECONOMY.name(),new FlightTravelClass(1,1,TravelClassType.ECONOMY,70) );
        travelClassDetails.put(TravelClassType.FIRSTCLASS.name(),new FlightTravelClass(2,1,TravelClassType.FIRSTCLASS,10) );
        travelClassDetails.put(TravelClassType.BUSINESS.name(), new FlightTravelClass(3,1,TravelClassType.BUSINESS,20) );
        fs.setTravelClassDetails( travelClassDetails );
        existingScheduledFlights.put( 1, fs );

        FlightRepository flightRepository = mock( FlightRepository.class );
        when( flightRepository.getAllScheduledDetails() ).thenReturn( existingScheduledFlights );

        FlightServices flightServices = new FlightServices( flightRepository );

        FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
        searchCriteria.setSource( "Hyderabad" );
        searchCriteria.setDestination( "Delhi" );
        searchCriteria.setTravelDate( "08/26/2019" );
        searchCriteria.setTravelClass( TravelClassType.ECONOMY.name() );
        searchCriteria.setNoOfSeats( 2 );

        List<ScheduleResponseDetails> results = flightServices.searchFlight( searchCriteria );
        assertThat(results).hasSize(1);
        assertThat(results).extracting("flightScheduleId").containsExactly(1);
        assertThat(results).extracting("totalPrice").containsExactly(11600.0);

    }

}
